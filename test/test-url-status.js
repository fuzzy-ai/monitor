// test-url-status.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('vows')
const {assert} = vows
const {Databank, DatabankObject} = require('databank')
const async = require('async')
const _ = require('lodash')

vows.describe('URLStatus')
  .addBatch({
    'When we set up the database': {
      topic () {
        const db = Databank.get('memory', {})
        db.connect({}, (err) => {
          if (err) {
            this.callback(err)
          } else {
            DatabankObject.bank = db
            this.callback(null, db)
          }
        })
      },
      'it works' (err, db) {
        assert.ifError(err)
        assert.isObject(db)
      },
      teardown (db) {
        db.disconnect(this.callback)
      },
      'and we import the URLStatus module': {
        topic () {
          return require('../lib/url-status')
        },
        'it works' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
        },
        'it has a schema property' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
          assert.isObject(URLStatus.schema)
        },
        'it has a type property' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
          assert.isString(URLStatus.type)
        },
        'it has a maybeGet method' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
          assert.isFunction(URLStatus.maybeGet)
        },
        'it has a create method' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
          assert.isFunction(URLStatus.create)
        },
        'it has a readAll method' (err, URLStatus) {
          assert.ifError(err)
          assert.isFunction(URLStatus)
          assert.isFunction(URLStatus.readAll)
        },
        'and we create a few URLStatuses': {
          topic (URLStatus) {
            const create = (i, callback) => {
              const url = `http://url${i}.test/endpoint`
              URLStatus.create({url: url, up: true, lastMessage: 'test'}, callback)
            }
            async.times(10, create, this.callback)
          },
          'it works' (err, statuses) {
            assert.ifError(err)
            assert.isArray(statuses)
            assert.lengthOf(statuses, 10)
            let i = 0
            for (i in statuses) {
              assert.isObject(statuses[i])
              assert.equal(statuses[i].url, `http://url${i}.test/endpoint`)
              assert.isTrue(statuses[i].up)
              assert.equal(statuses[i].lastMessage, 'test')
              assert.isString(statuses[i].created)
              assert.isString(statuses[i].updated)
              assert.equal(statuses[i].created, statuses[i].updated)
            }
          },
          'and we maybeGet() one that exists': {
            topic (statuses, URLStatus) {
              URLStatus.maybeGet('http://url0.test/endpoint', this.callback)
            },
            'it returns a valid status' (err, status) {
              assert.ifError(err)
              assert.isObject(status)
              assert.equal(status.url, 'http://url0.test/endpoint')
              assert.isTrue(status.up)
              assert.equal(status.lastMessage, 'test')
              assert.isString(status.created)
              assert.isString(status.updated)
              assert.equal(status.created, status.updated)
            }
          },
          "and we maybeGet() one that doesn't exist": {
            topic (statuses, URLStatus) {
              URLStatus.maybeGet('http://nonexistent.test/endpoint', this.callback)
            },
            'it returns null' (err, status) {
              assert.ifError(err)
              assert.isNull(status)
            }
          },
          'and we update one': {
            topic (statuses, URLStatus) {
              assert(statuses[1].url === 'http://url1.test/endpoint')
              statuses[1].update({up: false, lastMessage: 'error'}, this.callback)
            },
            'it returns the updated status' (err, status) {
              assert.ifError(err)
              assert.isObject(status)
              assert.equal(status.url, 'http://url1.test/endpoint')
              assert.isFalse(status.up)
              assert.equal(status.lastMessage, 'error')
              assert.isString(status.created)
              assert.isString(status.updated)
              assert.notEqual(status.created, status.updated)
            }
          },
          'and we readAll()': {
            topic (statuses, URLStatus) {
              const urls = statuses.map((status) => status.url)
              URLStatus.readAll(urls, this.callback)
            },
            'it returns all the statuses' (err, statuses) {
              assert.ifError(err)
              assert.isObject(statuses)
              assert.lengthOf(_.values(statuses), 10)
              let url
              for (url in statuses) {
                const status = statuses[url]
                assert.isObject(status)
                assert.equal(status.url, url)
                assert.isBoolean(status.up)
                assert.isString(status.lastMessage)
                assert.isString(status.created)
                assert.isString(status.updated)
              }
            }
          }
        }
      }
    }
  })
  .export(module)
