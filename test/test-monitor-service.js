// test-monitor-service.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const vows = require('vows')
const {assert} = vows
const async = require('async')
const debug = require('debug')('monitor/test-monitor-service')

vows.describe('MonitorService')
  .addBatch({
    'When we import the MonitorService module': {
      topic () {
        return require('../lib/monitor-service')
      },
      'it works' (err, MonitorService) {
        assert.ifError(err)
        assert.isFunction(MonitorService)
      },
      'and we create a MonitorService': {
        topic (MonitorService) {
          const env = {
            PORT: 2342,
            DRIVER: 'memory',
            PARAMS: '{}',
            DELAY: 2,
            SHORT_DELAY: 1,
            SHORT_CHECKS: 3
          }
          return new MonitorService(env)
        },
        'it works' (err, svc) {
          assert.ifError(err)
          assert.isObject(svc)
        },
        'it has a start() method' (err, svc) {
          assert.ifError(err)
          assert.isObject(svc)
          assert.isFunction(svc.start)
        },
        'it has a stop() method' (err, svc) {
          assert.ifError(err)
          assert.isObject(svc)
          assert.isFunction(svc.stop)
        }
      },
      'and we run a MonitorService': {
        topic (MonitorService) {
          const called = {
            server: 0,
            slack: 0
          }
          const server = http.createServer((req, res) => {
            debug('http server hit')
            called.server++
            res.writeHead(200, { 'Content-Type': 'text/plain' })
            res.end('ok')
          })
          const slack = http.createServer((req, res) => {
            debug('slack server hit')
            called.slack++
            res.writeHead(200, { 'Content-Type': 'text/plain' })
            res.end('ok')
          })
          const env = {
            URL0: 'http://localhost:1516/',
            URL1: 'http://localhost:2308/',
            PORT: 2342,
            DRIVER: 'memory',
            PARAMS: '{}',
            DELAY: 2,
            SHORT_DELAY: 1,
            SHORT_CHECKS: 3,
            SLACK_HOOK: 'http://localhost:4208'
          }
          const svc = new MonitorService(env)

          async.waterfall([
            (callback) => {
              debug('starting server on 1516')
              server.listen(1516, callback)
            },
            (callback) => {
              debug('starting slack server on 4208')
              slack.listen(4208, callback)
            },
            (callback) => {
              debug('starting MonitorService')
              svc.start(callback)
            },
            (callback) => {
              debug('waiting 2 seconds')
              setTimeout(callback, 2000)
            },
            (callback) => {
              debug('stopping MonitorService')
              svc.stop(callback)
            },
            (callback) => {
              slack.close(callback)
            },
            (callback) => {
              server.close(callback)
            }
          ], (err) => {
            debug('finished')
            if (err) {
              this.callback(err)
            } else {
              this.callback(null, called)
            }
          })
        },
        'it works' (err, called) {
          assert.ifError(err)
          assert.isObject(called)
        },
        'server was pinged several times' (err, called) {
          assert.ifError(err)
          assert.isObject(called)
          assert.greater(called.server, 0)
        },
        'slack hook was called' (err, called) {
          assert.ifError(err)
          assert.isObject(called)
          assert.greater(called.slack, 0)
        }
      }
    }
  })
  .export(module)
