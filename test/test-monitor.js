// test-monitor.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const vows = require('vows')
const {assert} = vows
const {Databank, DatabankObject} = require('databank')
const async = require('async')
const debug = require('debug')('monitor/test-monitor')

vows.describe('Monitor')
  .addBatch({
    'When we set up the database': {
      topic () {
        const db = Databank.get('memory', {})
        db.connect({}, (err) => {
          if (err) {
            this.callback(err)
          } else {
            DatabankObject.bank = db
            this.callback(null, db)
          }
        })
      },
      'it works' (err, db) {
        assert.ifError(err)
        assert.isObject(db)
      },
      'it is the one used by URLStatus' (err, db) {
        assert.ifError(err)
        assert.isObject(db)
        assert.equal(require('../lib/url-status').bank(), db)
      },
      teardown (db) {
        db.disconnect(this.callback)
      },
      'and we import the Monitor module': {
        topic () {
          return require('../lib/monitor')
        },
        'it works' (err, Monitor) {
          assert.ifError(err)
          assert.isFunction(Monitor)
        },
        'and we create a monitor': {
          topic (Monitor) {
            const config = {
              urls: ['http://localhost:2342/'],
              delay: 1,
              shortDelay: 0.1,
              shortChecks: 10
            }
            const serverUp = (url, callback) => { callback() }
            const serverDown = (url, err, callback) => { callback() }
            return new Monitor(config, serverUp, serverDown)
          },
          'it works' (err, monitor) {
            assert.ifError(err)
            assert.isObject(monitor)
          },
          'it has a start() method' (err, monitor) {
            assert.ifError(err)
            assert.isObject(monitor)
            assert.isFunction(monitor.start)
          },
          'it has a stop() method' (err, monitor) {
            assert.ifError(err)
            assert.isObject(monitor)
            assert.isFunction(monitor.stop)
          }
        },
        'and we run a monitor': {
          topic (Monitor) {
            const called = {
              server: 0,
              up: 0,
              down: 0
            }
            const server = http.createServer((req, res) => {
              debug('http server hit')
              called.server++
              res.writeHead(200, { 'Content-Type': 'text/plain' })
              res.end('ok')
            })
            const serverUp = (url, callback) => {
              debug('serverUp() called')
              called.up++
              callback(null)
            }
            const serverDown = (url, err, callback) => {
              debug('serverDown() called')
              called.down++
              callback(null)
            }
            const config = {
              urls: ['http://localhost:1516/', 'http://localhost:2308/'],
              delay: 0.1,
              shortDelay: 0.05,
              shortChecks: 10
            }
            const monitor = new Monitor(config, serverUp, serverDown)

            async.waterfall([
              (callback) => {
                debug('starting server on 1516')
                server.listen(1516, callback)
              },
              (callback) => {
                debug('starting monitor')
                monitor.start(callback)
              },
              (callback) => {
                debug('waiting 2 seconds')
                setTimeout(callback, 2000)
              },
              (callback) => {
                debug('stopping monitor')
                monitor.stop(callback)
              },
              (callback) => {
                server.close(callback)
              }
            ], (err) => {
              debug('finished')
              if (err) {
                this.callback(err)
              } else {
                this.callback(null, called)
              }
            })
          },
          'it works' (err, called) {
            assert.ifError(err)
            assert.isObject(called)
          },
          'server was pinged several times' (err, called) {
            assert.ifError(err)
            assert.isObject(called)
            assert.greater(called.server, 4)
          },
          'serverUp callback was called once' (err, called) {
            assert.ifError(err)
            assert.isObject(called)
            assert.greater(called.up, 0)
            assert.equal(called.up, 1)
          },
          'serverDown callback was called once' (err, called) {
            assert.ifError(err)
            assert.isObject(called)
            assert.greater(called.down, 0)
            assert.equal(called.down, 1)
          }
        }
      }
    }
  })
  .export(module)
