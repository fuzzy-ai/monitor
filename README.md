# monitor

Monitoring server for Fuzzy.ai.

## License

Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Environment variables

This is an instance of [Microservice](https://gitlab.com/fuzzy-ai/microservice/)
so all the environment variables that work there work here.

In addition, it uses the following variables:

  - **URL0, URL1, URL2...**  - Which URLs to check. It should work with any
  digits after the `URL` part, but I haven't tried much more than 9. Must be a
  full URL; must work with `GET` HTTP verb.

  - **DELAY**  - How long to wait between checks. Defaults to 60 seconds.

  - **SHORT_DELAY**  - How long to wait between checks of URLs that have
  recently changed state. Defaults to 10 seconds.

  - **SHORT_CHECKS**  - How many times to do short-delay checks. Defaults to 10.
  After this, the checks go back to the regular check period.
