// main.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const MonitorService = require('./monitor-service')

const service = new MonitorService(process.env)

service.start((err) => {
  if (err) {
    console.error(err)
    return process.exit(-1)
  } else {
    return console.log(`${service.getName()} started.`)
  }
})

// Try to send a Slack message on error

process.on('uncaughtException', (err) => {
  if ((service != null) && (service.slackMessage != null)) {
    const msg = `UNCAUGHT EXCEPTION: ${err.message}`
    return service.slackMessage('error', msg, ':bomb:', (err) => {
      if (err != null) {
        return console.error(err)
      }
    })
  }
})

const shutdown = function () {
  console.log('Shutting down...')
  return service.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(-1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)
