// urlstatus.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const assert = require('assert')
const _ = require('lodash')
const debug = require('debug')('monitor/url-status')

const {DatabankObject} = require('databank')

const URLStatus = DatabankObject.subClass('URLStatus')

URLStatus.schema = {
  pkey: 'url',
  fields: [
    'up',
    'lastMessage',
    'created',
    'updated'
  ]
}

URLStatus.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.url))
    assert(_.isBoolean(props.up))
    assert((props.lastMessage == null) || _.isString(props.lastMessage))
  } catch (err) {
    callback(err)
  }

  props.created = (props.updated = (new Date()).toISOString())

  return callback(null, props)
}

URLStatus.prototype.beforeUpdate = function (props, callback) {
  props.updated = (new Date()).toISOString()

  return callback(null, props)
}

URLStatus.prototype.beforeSave = function (callback) {
  if ((this.created == null)) {
    return URLStatus.beforeCreate(this, callback)
  } else {
    return this.beforeUpdate(this, callback)
  }
}

URLStatus.maybeGet = (url, callback) => {
  debug(`maybeGet(${url})`)
  URLStatus.get(url, (err, status) => {
    if (err && (err.name === 'NoSuchThingError')) {
      debug(`NoSuchThingError for ${url}`)
      return callback(null, null)
    } else if (err) {
      debug(`Error maybeGetting ${url}`)
      return callback(err)
    } else {
      debug(`maybeGot ${url}`)
      return callback(null, status)
    }
  })
}

module.exports = URLStatus
