// monitorservice.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const fs = require('fs')
const path = require('path')

const Microservice = require('@fuzzy-ai/microservice')
const _ = require('lodash')
const debug = require('debug')('monitor/service')
const {DatabankObject} = require('databank')

const URLStatus = require('./url-status')
const Monitor = require('./monitor')

class MonitorService extends Microservice {
  getName () {
    return 'monitor'
  }

  getSchema () {
    const schema = {}
    schema[URLStatus.type] = URLStatus.schema
    return schema
  }

  getVersion () {
    if ((this.version == null)) {
      const pkg = fs.readFileSync(path.join(__dirname, '..', 'package.json'))
      const data = JSON.parse(pkg)
      this.version = data.version
    }
    return this.version
  }

  setupRoutes (exp) {
    exp.get('/', this._showURLStatuses.bind(this))
    exp.get('/version', this._showVersion.bind(this))

    return exp
  }

  startCustom (callback) {
    // XXX: it's not clear how or why this is happening but :shrug:
    if (!DatabankObject.bank && this.db) {
      DatabankObject.bank = this.db
    }
    this.monitor = new Monitor(this.config, this.serverUp.bind(this), this.serverDown.bind(this))

    this.monitor.start(err => callback(err))
  }

  stopCustom (callback) {
    this.monitor.stop(err => callback(err))
  }

  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)

    cfg.urls = _.filter(env, (value, key) => key.match(/^URL\d+/))
    cfg.delay = this.envInt(env, 'DELAY', 60)
    cfg.shortDelay = this.envInt(env, 'SHORT_DELAY', 10)
    cfg.shortChecks = this.envInt(env, 'SHORT_CHECKS', 10)

    return cfg
  }

  serverUp (url, callback) {
    const text = `URL <${url}> is up.`
    this.slackMessage('monitor', text, ':beers:', callback)
  }

  serverDown (url, err, callback) {
    const text = `URL <${url}> is down: ${err.message}.`
    this.slackMessage('monitor', text, ':fire:', callback)
  }

  _showVersion (req, res, next) {
    res.json({name: this.getName(), version: this.getVersion()})
  }

  _showURLStatuses (req, res, next) {
    debug(this.config.urls)
    URLStatus.readAll(this.config.urls, (err, statuses) => {
      if (err) {
        return next(err)
      } else {
        debug(statuses)
        let buf = 'URL\tState\tLast checked\n'
        for (const url in statuses) {
          const status = statuses[url]
          if (status != null) {
            const state = status.up ? 'UP' : 'DOWN'
            buf += `${url}\t${state}\t${status.updated}\n`
          } else {
            buf += `${url}\tUNKNOWN\tNEVER\n`
          }
        }
        res.set('Content-Type', 'text/plain')
        return res.send(buf)
      }
    })
  }
}

module.exports = MonitorService
