// monitor.js
// Copyright 2014-2018, 9165584 Canada Corporation <mailto:legal@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// <http://www.apache.org/licenses/LICENSE-2.0>
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const assert = require('assert')
const fs = require('fs')
const path = require('path')

const _ = require('lodash')
const async = require('async')
const {get} = require('@fuzzy-ai/web')
const debug = require('debug')('monitor/monitor')

const URLStatus = require('./url-status')

class Monitor {
  constructor (config, serverUp, serverDown) {
    this._checkUrl = this._checkUrl.bind(this)
    this._checkURLs = this._checkURLs.bind(this)
    this._checkRecentURLs = this._checkRecentURLs.bind(this)
    this.serverUp = serverUp
    this.serverDown = serverDown
    this.urls = config.urls
    this.delay = config.delay
    this.shortDelay = config.shortDelay
    this.shortChecks = config.shortChecks

    assert(_.isArray(this.urls), 'URLs argument must be an array of URLs')
    assert(_.every(this.urls, _.isString), 'All URLS must be strings')
    assert(_.isFinite(this.delay) && (this.delay > 0),
      'Delay argument must be positive number')
    assert(_.isFinite(this.shortDelay) && (this.shortDelay > 0),
      'Short delay argument must be positive number')
    assert(_.isFinite(this.shortChecks) && (this.shortChecks > 0),
      'Short checks argument must be positive number')
    assert(_.isFunction(this.serverUp), 'serverUp callback must be a function')
    assert(_.isFunction(this.serverDown), 'serverDown callback must be a function')

    this.interval = null
    this.shortInterval = null

    this.version = this._getVersion()

    debug(`Version=${this.version}`)

    this.recentURLs = {}
  }

  start (callback) {
    debug('Starting')
    this._checkURLs()
    this.interval = setInterval(this._checkURLs, this.delay * 1000)
    debug(`Interval = ${this.interval}`)
    this.shortInterval = setInterval(this._checkRecentURLs, this.shortDelay * 1000)
    debug(`Short interval = ${this.shortInterval}`)
    callback(null)
  }

  stop (callback) {
    debug(`Interval = ${this.interval}`)

    clearInterval(this.interval)
    clearInterval(this.shortInterval)

    callback(null)
  }

  _getVersion () {
    const pkg = fs.readFileSync(path.join(__dirname, '..', 'package.json'))
    const data = JSON.parse(pkg)
    return data.version
  }

  _checkUrl (url, callback) {
    const headers =
      {'User-Agent': `fuzzy.ai-monitor/${this.version}`}
    debug(`Getting ${url}...`)
    get(url, headers, (err, res, body) => {
      if (err) {
        debug(`Error getting ${url}: ${err.message}`)
        this._handleUrlDown(url, err, callback)
      } else {
        debug(`URL ${url} is OK`)
        this._handleUrlUp(url, callback)
      }
    })
  }

  _handleUrlDown (url, err, callback) {
    let last = null

    async.waterfall([
      function (callback) {
        debug(`Getting last status for ${url}`)
        URLStatus.maybeGet(url, callback)
      },
      (results, callback) => {
        last = results
        debug(`Last status = ${last} for ${url}`)
        if (last != null) {
          debug(`Updating last status for ${url}`)
          last.update({up: false}, callback)
        } else {
          debug(`Creating new status for ${url}`)
          URLStatus.create({url, up: false}, callback)
        }
      },
      (updated, callback) => {
        debug(`updated.url = ${updated.url}, updated.up = ${updated.up}`)
        if ((last == null) || last.up) {
          debug(`Reporting server down for ${url}`)
          this.recentURLs[url] = 1
          this.serverDown(url, err, callback)
        } else {
          this._noteRecentURL(url)
          debug(`Not reporting server down for ${url}`)
          callback(null)
        }
      }
    ], (err) => {
      if (err) {
        debug(`Error handling server down: ${err}`)
        callback(err)
      } else {
        debug(`Done with new status`)
        callback(null)
      }
    })
  }

  _handleUrlUp (url, callback) {
    let last = null

    async.waterfall([
      function (callback) {
        debug(`Getting last status for ${url}`)
        URLStatus.maybeGet(url, callback)
      },
      (results, callback) => {
        last = results
        debug(`Last status = ${last} for ${url}`)
        if (last != null) {
          debug(`Updating last status for ${url}`)
          last.update({up: true}, callback)
        } else {
          debug(`Creating new status for ${url}`)
          URLStatus.create({url, up: true}, callback)
        }
      },
      (updated, callback) => {
        debug(`updated.url = ${updated.url}, updated.up = ${updated.up}`)
        if ((last == null) || !last.up) {
          debug(`Reporting server up for ${url}`)
          this.recentURLs[url] = 1
          this.serverUp(url, callback)
        } else {
          this._noteRecentURL(url)
          debug(`Not reporting server up for ${url}`)
          callback(null)
        }
      }
    ], (err, results) => {
      if (err) {
        debug(`Error handling server up: ${err}`)
        callback(err)
      } else {
        debug(`Done with new status`)
        callback(null)
      }
    })
  }

  _noteRecentURL (url) {
    if (this.recentURLs[url]) {
      debug('Another short delay without changes')
      this.recentURLs[url] += 1
      if (this.recentURLs[url] > this.shortChecks) {
        debug(`Removing ${url} from list of urls for short delay`)
        delete this.recentURLs[url]
      }
    }
  }

  _checkURLs () {
    debug('Beginning _checkURLs')

    async.each(this.urls, this._checkUrl, (err) => {
      debug('After async.each()')
      if (err) {
        debug(`_checkURLs got an error: ${err}`)
        console.error(err)
      }
    })
  }

  _checkRecentURLs () {
    debug('Beginning _checkRecentURLs')

    async.each(_.keys(this.recentURLs), this._checkUrl, (err) => {
      debug('After async.each()')
      if (err) {
        debug(`_checkURLs got an error: ${err}`)
        console.error(err)
      }
    })
  }
}

module.exports = Monitor
