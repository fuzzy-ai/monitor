FROM node:10-alpine

RUN apk add --no-cache curl

WORKDIR /opt/monitor
COPY . /opt/monitor

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/monitor/bin/dumb-init", "--"]
CMD ["npm", "start"]
